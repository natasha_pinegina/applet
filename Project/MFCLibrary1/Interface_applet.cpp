// Interface_applet.cpp: ���� ����������
//

#include "stdafx.h"
#include "MFCLibrary1.h"
#include "Interface_applet.h"
#include "afxdialogex.h"
#include "windows.h"
#include "mmsystem.h"


// ���������� ���� Interface_applet

IMPLEMENT_DYNAMIC(Interface_applet, CDialog)

Interface_applet::Interface_applet(CWnd* pParent /*=NULL*/)
	: CDialog(Interface_applet::IDD, pParent)
{
}

Interface_applet::~Interface_applet()
{
}

void Interface_applet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Interface_applet, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &Interface_applet::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Interface_applet::OnBnClickedButton2)
END_MESSAGE_MAP()


// ����������� ��������� Interface_applet




void Interface_applet::OnBnClickedButton1()
{
	UpdateData(true);
	ShellExecute(NULL, L"open", L"cmd.exe", L"/?", NULL, SW_SHOWNORMAL);
	UpdateData(false);
}


void Interface_applet::OnBnClickedButton2()
{
	UpdateData(true);
	system("calc");
	UpdateData(false);
}
