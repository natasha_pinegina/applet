﻿
// cpl_loaderDlg.h: файл заголовка
//

#pragma once
#include <cpl.h>
#include "afxwin.h"

// Диалоговое окно CcplloaderDlg
class CcplloaderDlg : public CDialogEx
{
// Создание
public:
	CcplloaderDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CPL_LOADER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString Adress;
	CString Name;
	CString Opisanie;
	CStatic Picture;
	CButton Do;
	afx_msg void OnBnClickedObzor();
	afx_msg void OnBnClickedDo();
	HINSTANCE loadlib, HandleСpl;
	APPLET_PROC ProcAdr;
};
